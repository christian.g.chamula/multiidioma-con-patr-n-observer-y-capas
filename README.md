USE [master]
GO
/****** Object:  Database [EjemploArquitectura]    Script Date: 19/07/2023 22:36:33 ******/
CREATE DATABASE [EjemploArquitectura]

GO
USE [EjemploArquitectura]
GO
/****** Object:  Table [dbo].[Idiomas]    Script Date: 19/07/2023 22:36:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Idiomas](
	[id_idioma] [uniqueidentifier] NOT NULL,
	[nombre] [nvarchar](100) NOT NULL,
	[idioma_default] [bit] NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Usuarios]    Script Date: 19/07/2023 22:36:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Usuarios](
	[id_usuario] [uniqueidentifier] NOT NULL,
	[username] [nvarchar](100) NOT NULL,
	[password] [nvarchar](100) NOT NULL,
	[id_idioma] [uniqueidentifier] NOT NULL,
 CONSTRAINT [PK_Usuarios] PRIMARY KEY CLUSTERED 
(
	[id_usuario] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
USE [master]
GO
ALTER DATABASE [EjemploArquitectura] SET  READ_WRITE 
GO
